const { PrismaClient } = require('@prisma/client');


const prisma = new PrismaClient();

async function createList(req,res){

//Create a List

    const list= await prisma.list.create({

        data:{

            name:req.body.name,
            userId:req.body.userId,
            description:req.body.description,
        }

    })

    res.send(list)

}

//Find Lists by User Id

async function findLists(req,res){
        
        const lists=await prisma.list.findMany({

            where:{
                userId: parseInt(req.params.id),
            },
    
        })
 
    res.send(lists);

}


//Find List By Id

async function findListById(req,res){

    const list= await prisma.list.findUnique({

        where:{

            id: parseInt(req.params.id)

        },
        include:{

            songs:true,
        }

    })


    res.send(list)

}

//Add Songs to a List

 async function addSongs(req,res){


    const song= await prisma.listToSong.create({

        data:{   
            
            listId: parseInt(req.body.listId),
            songId:req.body.songId,

        }

    })       
    res.send(song);
}

//Eliminate a List

async function eliminateList(req,res){

    const list= await prisma.list.delete({

        where:{

            id: parseInt(req.params.id)

        }

    })

    res.send(list);

}

//Functions exported

module.exports={

        createList,
        findLists,
        findListById,
        addSongs, 
        eliminateList
    
}