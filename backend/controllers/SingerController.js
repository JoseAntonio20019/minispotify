const { PrismaClient } = require('@prisma/client');


const prisma = new PrismaClient();

//Create a Singer

async function createSinger(req,res){

    const singer= await prisma.singer.create({
        
        data:{
                name: req.body.name,
                age:req.body.age,
            

    }

})

res.send(singer);
    
}

//Find all the Singers

async function findSingers(req,res){

    const singers= await prisma.singer.findMany();

    res.send(singers);

}

//Find Singer By Id

async function findSingerById(req,res){

    const singer=await prisma.singer.findUnique({

        where:{
            id: parseInt(req.params.id)
        }
    })

    res.send(singer);


}

//Update Singer

async function updateSinger(req,res){

    const singer= await prisma.singer.update({

        where:{ id: parseInt(req.params.id)},
        data:{
            name:req.body.name,
            age:req.body.age,
            

        },
        
    })

    res.send(singer);

}

//Delete Singer

async function eliminateSinger(req,res){


    const singer= await prisma.singer.delete({

        where:{ id: parseInt(req.params.id)}

    })
    res.send(singer);
}
;

//Functions exported
module.exports={ 
             createSinger,
             findSingers,
             findSingerById,
             updateSinger,
             eliminateSinger
};