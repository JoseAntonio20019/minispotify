const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();


//Create song
async function createSong(req, res) {
  
  const song = await prisma.song.create({

    data: {

        songname: req.body.songname,
        genre: req.body.genre,
        authorName: req.body.authorName,
      
    },
  });

  res.send(song);
}

//Find all the Songs
async function findSongs(req, res) {

  const songs = await prisma.song.findMany();

  res.send(songs);
}

//Find Song By Id
async function findSongById(req, res) {

  const song = await prisma.song.findUnique({
    where: {
      id: parseInt(req.params.id),
    },
  });

  res.send(song);
}

//Update Song
async function updateSong(req, res) {
  const song = await prisma.song.update({
    where: { id: parseInt(req.params.id) },
    data: {
      songname: req.body.songname,
      genre: req.body.genre,
      authorName: req.body.authorName,
    },
  });

  res.send(song);
}

//Eliminate Song
async function eliminateSong(req, res) {
  const song = await prisma.song.delete({
    where: { id: parseInt(req.params.id) },
  });
  res.send(song);
}

//Functions exported 

module.exports = {
  createSong,
  findSongs,
  findSongById,
  updateSong,
  eliminateSong,
};
