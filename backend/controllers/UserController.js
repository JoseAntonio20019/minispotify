const { PrismaClient } = require('@prisma/client');
const res = require('express/lib/response');
const prisma = new PrismaClient();


    async function create (req,res){

        const user= await prisma.user.create({
            
            data:{
                    name: req.body.name,
                    email:req.body.email,
                    isAdmin:false,
                   

        }

    })

    res.send(user);
        
    }

    async function allUsers(req,res){


        const users= await prisma.user.findMany();


        res.send(users);

    }

    async function findById(req,res){

        const user=await prisma.user.findUnique({

            where:{

                id:parseInt(req.params.id),
            },
        })
        

        res.send(user);


    }

    async function findByEmail(req,res){

        const user= await prisma.user.findUnique({

            where:{

                email:req.params.email,

            }

        })

        res.send(user);

    }


    async function update(req,res){

        

        const user= await prisma.user.update({

            where:{ id:parseInt(req.params.id)},
            data:{
                name:req.body.name,
                email:req.body.email,

            },
            
        })

        res.send(user);

    }

    async function eliminate(req,res){


        const user= await prisma.user.delete({

            where:{ id:parseInt(req.params.id)}

        })
        res.send(user);
    }
;
    
//Functions exported

module.exports={ 
                 create,
                 allUsers,
                 findById,
                 findByEmail,
                 update,
                 eliminate
};
