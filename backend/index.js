//Requires
const { urlencoded } = require('express');
const express = require('express');
const app = express();
const port = 3000;
const UserController= require("./routes/user");
const SongController= require("./routes/song");
const SingerController= require("./routes/singer");
const ListController= require("./routes/list");
const cors=require('cors');

//App middlewares
app.use(cors());
app.use(express.urlencoded({extended:true}));

app.use(express.json());

//Backend Controllers

app.use(ListController);
app.use(UserController);
app.use(SongController);
app.use(SingerController);



//Port 
app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})