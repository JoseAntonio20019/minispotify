/*
  Warnings:

  - You are about to drop the `listtosong` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `listtosong` DROP FOREIGN KEY `listtosong_ibfk_1`;

-- DropForeignKey
ALTER TABLE `listtosong` DROP FOREIGN KEY `listtosong_ibfk_2`;

-- DropTable
DROP TABLE `listtosong`;

-- CreateTable
CREATE TABLE `_ListToSong` (
    `A` INTEGER NOT NULL,
    `B` INTEGER NOT NULL,

    UNIQUE INDEX `_ListToSong_AB_unique`(`A`, `B`),
    INDEX `_ListToSong_B_index`(`B`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `_ListToSong` ADD FOREIGN KEY (`A`) REFERENCES `List`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_ListToSong` ADD FOREIGN KEY (`B`) REFERENCES `Song`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
