/*
  Warnings:

  - You are about to drop the `_listtosong` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `_listtosong` DROP FOREIGN KEY `_listtosong_ibfk_1`;

-- DropForeignKey
ALTER TABLE `_listtosong` DROP FOREIGN KEY `_listtosong_ibfk_2`;

-- DropTable
DROP TABLE `_listtosong`;

-- CreateTable
CREATE TABLE `ListToSong` (
    `listId` INTEGER NOT NULL,
    `songId` INTEGER NOT NULL,

    PRIMARY KEY (`listId`, `songId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `ListToSong` ADD CONSTRAINT `ListToSong_songId_fkey` FOREIGN KEY (`songId`) REFERENCES `Song`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `ListToSong` ADD CONSTRAINT `ListToSong_listId_fkey` FOREIGN KEY (`listId`) REFERENCES `List`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
