-- DropForeignKey
ALTER TABLE `list` DROP FOREIGN KEY `List_userId_fkey`;

-- DropForeignKey
ALTER TABLE `listtosong` DROP FOREIGN KEY `ListToSong_listId_fkey`;

-- DropForeignKey
ALTER TABLE `listtosong` DROP FOREIGN KEY `ListToSong_songId_fkey`;

-- DropForeignKey
ALTER TABLE `song` DROP FOREIGN KEY `Song_authorName_fkey`;

-- AddForeignKey
ALTER TABLE `Song` ADD CONSTRAINT `Song_authorName_fkey` FOREIGN KEY (`authorName`) REFERENCES `Singer`(`name`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `List` ADD CONSTRAINT `List_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `ListToSong` ADD CONSTRAINT `ListToSong_songId_fkey` FOREIGN KEY (`songId`) REFERENCES `Song`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `ListToSong` ADD CONSTRAINT `ListToSong_listId_fkey` FOREIGN KEY (`listId`) REFERENCES `List`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
