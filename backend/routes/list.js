const express=require('express');
const ListController= require("../controllers/ListController");

const router=express.Router();


//Routes 

router.get('/lists/:id',ListController.findLists)
      .get('/list/:id',ListController.findListById)
      .post('/createlist',ListController.createList)
      .post('/addSongs',ListController.addSongs) 
      .post('/deleteList/:id',ListController.eliminateList)


module.exports=router;
