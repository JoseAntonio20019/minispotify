const express=require("express");
const SingerController= require("../controllers/SingerController");

const router=express.Router();

//Singer Routes

router.get('/singers',SingerController.findSingers)
      .get('/singer/:id',SingerController.findSingerById)
      .get('/updatesinger/:id',SingerController.updateSinger)
      .post('/createsinger',SingerController.createSinger)
      .post('/updatesinger/:id',SingerController.updateSinger)
      .post('/deletesinger/:id',SingerController.eliminateSinger)

module.exports=router;