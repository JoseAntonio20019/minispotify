const express= require("express");
const SongController= require("../controllers/SongController");

const router=express.Router();

//Song Routes

router.get("/songs",SongController.findSongs)
      .post("/createsong",SongController.createSong)
      .get("/song/:id",SongController.findSongById)
      .get("/updatesong/:id",SongController.updateSong)
      .post("/updatesong/:id",SongController.updateSong)
      .post("/deletesong/:id",SongController.eliminateSong)

module.exports=router;