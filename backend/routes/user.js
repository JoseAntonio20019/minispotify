const express= require("express");
const UserController= require("../controllers/UserController");

const router= express.Router();

//User Routes

router.get('/users',UserController.allUsers)
      .get('/user/:id',UserController.findById)
      .get('/useremail/:email',UserController.findByEmail)
      .get('/updateuser/:id',UserController.update)
      .post('/createuser',UserController.create)
      .post('/updateuser/:id',UserController.update)
      .post('/deleteuser/:id',UserController.eliminate)

      module.exports=router;

    