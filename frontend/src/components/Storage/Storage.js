import { defineStore } from "pinia";
import { ref } from "vue";

//Define a store with pinia

export const URL = defineStore("URL",{

    state: () =>{


            //Consts
            const isAdmin=ref(false);
            const url='http://localhost:3000';
            const useremail=ref('');
            const uid=ref('');
            const username=ref('');

            //Export the store with the consts
        return{
            url,
            useremail,
            isAdmin,
            uid,
            username,
            
        }

    }

})

