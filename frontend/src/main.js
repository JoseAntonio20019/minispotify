//Imports 
import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router/index'


//Firebase
import { initializeApp } from "firebase/app";
const firebaseConfig = {
  apiKey: "AIzaSyBG5ZQOJhXzgmeJP9K9_-bakEIkKQSxIjU",
  authDomain: "dpl-dew.firebaseapp.com",
  projectId: "dpl-dew",
  storageBucket: "dpl-dew.appspot.com",
  messagingSenderId: "744904722808",
  appId: "1:744904722808:web:1c62bfe922d0148b89a08d",
  measurementId: "G-LT91FER3FC"
};

// Initialize Firebase
initializeApp(firebaseConfig);

//Define the app 
const app= createApp(App);

//App modules used
app.use(router); 
app.use(createPinia());
app.mount('#app');


