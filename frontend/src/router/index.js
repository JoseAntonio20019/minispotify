import { createRouter, createWebHistory } from "vue-router";

//Routes
import Home from "../components/views/Home.vue";
//Users

import Users from "../components/Users/Users.vue";
import EditUser from "../components/Users/EditUser.vue";
import Register from "../components/views/Register.vue";
import Login from "../components/views/Login.vue";

//Songs

import Songs from "../components/Songs/Songs.vue";
import AddSong from "../components/Songs/AddSong.vue";
import EditSong from "../components/Songs/EditSong.vue";

//Singers

import Singers from "../components/Singers/Singers.vue";
import AddSinger from "../components/Singers/AddSinger.vue";
import EditSinger from "../components/Singers/EditSinger.vue";

//Lists

import Lists from '../components/Lists/Lists.vue';
import AddList from '../components/Lists/AddList.vue';
import ViewList from '../components/Lists/ShowList.vue';
import AddSongs from '../components/Lists/AddSongs.vue'


//Router with all the routes used 

const router = createRouter({
  routes: [
    { path: "/", component: Home },

    //User Routes
    { path: "/users", component: Users },
    { path: "/users/edituser/:id", component: EditUser, requiresAuth: true },
    { path: "/register", component: Register},
    { path: "/login", component: Login },

    //Song Routes
    { path: "/songs", component: Songs },
    { path: "/addsong", component: AddSong },
    { path: "/songs/editsong/:id", component: EditSong },

    //Singer Routes
    { path: "/singers", component: Singers },
    { path: "/addsinger", component: AddSinger },
    { path: "/singers/editsinger/:id", component: EditSinger },

    //Lists Routes
    {path: "/lists/:id", component:Lists, requiresAuth: true},
    {path: "/list/:id", component:ViewList, requiresAuth: true},
    {path: "/createlist",component:AddList, requiresAuth: true},
    {path: "/addSongs/:id",component: AddSongs, requiresAuth: true}
  ],

  history: createWebHistory(),
});


//Functions 

//Get the current user logged
const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const removeListener = onAuthStateChanged(
      getAuth(),
      (user) => {
        removeListener();
        resolve(user);
      },
      reject
    );
  });
};


//Middleware to have access to specific routes 

router.beforeEach(async (to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (getAuth().currentUser) {
      next();
    } else {
      alert("you dont have access!");
      next("/");
    }
  } else {
    next();
  }
});


//Exports
export default router;
